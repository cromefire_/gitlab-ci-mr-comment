# GitLab CI MR Comment Bot
[![pipeline status](https://gitlab.com/cromefire_/gitlab-ci-mr-comment/badges/main/pipeline.svg)](https://gitlab.com/cromefire_/gitlab-ci-mr-comment/-/pipelines)

Allows you to post and update a comment on a merge from a CI job.

## Usage
```
gitlab-ci-mr-comment <path/to/template.tmpl> <old comment regex> [<key 1>=<value>] ... [<key n>=<value>]`
```
The template is just GitLab markdown with go templates in it.
You can define the template variables via the key-value arguments.

## Example

The `GITLAB_COMMENT_TOKEN` should be set to a personal access token for the account the bot should use. Be aware of the security implications.

`path/to/template.tmpl`:
```markdown
# This is a test comment
This is a test comment that will be updated with the pipeline id: {{.var}}
```

`.gitlab-ci.yml`:
```yaml
# [...]
some-job:
  only:
    refs:
      - merge_requests
  before_script:
    # Assumes you have wget in the container, but anything that can download a file will suffice
    - wget -q --https-only "https://gitlab.com/api/v4/projects/32900402/packages/generic/gitlab_ci_mr_comment/0.0.4/gitlab-ci-mr-comment"
    - chmod +x gitlab-ci-mr-comment
  script:
    - ./gitlab-ci-mr-comment path/to/template.tmpl "^# This is a test comment" var="${CI_PIPELINE_IID}"
```
