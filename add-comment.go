package main

import (
	"bytes"
	"github.com/xanzy/go-gitlab"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"text/template"
)

const minArgs = 2

func main() {
	if len(os.Args) <= minArgs {
		log.Fatalf("Needs a minimum of %d Arguments", minArgs)
	}
	token := os.Getenv("GITLAB_COMMENT_TOKEN")
	if token == "" {
		log.Fatalln("Missing token in env GITLAB_COMMENT_TOKEN")
	}
	templateFile := os.Args[1]
	regexStr := os.Args[2]
	gl, err := gitlab.NewClient(token)
	if err != nil {
		log.Fatalln("Failed to create gitlab client:", err)
	}
	midStr := os.Getenv("CI_MERGE_REQUEST_IID")
	if midStr == "" {
		log.Println("Not a merge request, skipping")
		os.Exit(0)
	}

	mid, err := strconv.ParseInt(midStr, 10, 32)
	if err != nil {
		log.Fatalln("Failed to parse merge request id:", err)
	}

	paramLen := len(os.Args) - (minArgs + 1)
	params := make(map[string]interface{})
	for i := 0; i < paramLen; i++ {
		spl := strings.SplitN(os.Args[i+(minArgs+1)], "=", 2)
		if len(spl) < 2 {
			log.Fatalf("Failed to parse parameter %d, missing '='", i+1)
		}
		params[spl[0]] = spl[1]
	}
	templateContent, err := ioutil.ReadFile(templateFile)
	if err != nil {
		log.Fatalln("Failed to read template:", err)
	}

	tmpl, err := template.New("comment").Parse(string(templateContent))
	if err != nil {
		log.Fatalln("Failed to parse template:", err)
	}

	var buf bytes.Buffer
	err = tmpl.Execute(&buf, params)
	if err != nil {
		log.Fatalln("Failed to render template:", err)
	}
	rendered := buf.String()
	reg, err := regexp.Compile(regexStr)
	if err != nil {
		log.Fatalln("Failed to compile old-comment-regex:", err)
	}

	if !reg.MatchString(rendered) {
		log.Println("Warning: Old-comment-regex doesn't match generated comment")
	}

	pidStr := os.Getenv("CI_PROJECT_ID")
	if pidStr == "" {
		log.Fatalln("Missing project id")
	}

	pid, err := strconv.ParseInt(pidStr, 10, 32)
	if err != nil {
		log.Fatalln("Failed to parse project id:", err)
	}

	user, _, err := gl.Users.CurrentUser()
	if err != nil {
		log.Fatalln("Failed to determine current user:", err)
	}

	sortAsc := "asc"
	notes, _, err := gl.Notes.ListMergeRequestNotes(int(pid), int(mid), &gitlab.ListMergeRequestNotesOptions{Sort: &sortAsc})
	if err != nil {
		log.Fatalln("Failed to fetch notes from gitlab:", err)
	}
	for i := range notes {
		note := notes[i]
		if reg.MatchString(note.Body) && note.Author.ID == user.ID {
			_, _, err := gl.Notes.UpdateMergeRequestNote(int(pid), int(mid), note.ID, &gitlab.UpdateMergeRequestNoteOptions{Body: &rendered})
			if err != nil {
				log.Fatalln("Failed to edit old comment:", err)
			}
			return
		}
	}

	_, _, err = gl.Notes.CreateMergeRequestNote(int(pid), int(mid), &gitlab.CreateMergeRequestNoteOptions{Body: &rendered})
	if err != nil {
		log.Fatalln("Failed to add new comment:", err)
	}
}
